package com.dinarys.api;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;

import static com.dinarys.api.Endpoints.ADMIN_TOKEN_URI;
import static com.dinarys.api.Endpoints.BASE_URL;

public class ApiConnector {
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "d1n2r3s4";
    private static String token;

    private static Response getToken(String userName, String password) {
        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("username", userName);
        requestParams.put("password", password);

        request.header("Content-Type", "application/json");
        request.body(requestParams.toJSONString());
        return request.post(ADMIN_TOKEN_URI);
    }

    public static String getAdminToken() {
        if (token == null) {
            Response response = getToken(USERNAME, PASSWORD);
            token = response.asString().replaceAll("\"","");
        }

        return token;
    }
}