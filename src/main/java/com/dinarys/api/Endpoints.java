package com.dinarys.api;

public class Endpoints {
    public static final String BASE_URL = "http://magento-demo.dinarys.com";
    public static final String ADMIN_TOKEN_URI = "/rest/V1/integration/admin/token";
    public static final String CUSTOMERS_URI = "/rest/V1/customers/";
}
