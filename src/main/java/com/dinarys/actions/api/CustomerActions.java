package com.dinarys.actions.api;

import com.dinarys.api.Endpoints;
import com.dinarys.api.models.Customer;
import io.qameta.allure.Step;

public class CustomerActions extends BaseApiActions {

    @Step
    public Customer getAdmin() {
        return prepareRequest().get(Endpoints.CUSTOMERS_URI +  "1").as(Customer.class);
    }

    @Step
    public Customer createCustomer(String email, String firstName) {
        String json = String.format("{\"customer\":{\"email\":\"%s\",\"firstname\":\"%s\",\"lastname\":\"TestAutomation\"},\n" +
                "  \"password\":\"Demo1234\"\n}", email, firstName);
        return prepareRequest().body(json).post(Endpoints.CUSTOMERS_URI).as(Customer.class);
    }
}
