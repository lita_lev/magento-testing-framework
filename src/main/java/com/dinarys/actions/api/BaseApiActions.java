package com.dinarys.actions.api;

import com.dinarys.api.ApiConnector;
import com.dinarys.api.Endpoints;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

public class BaseApiActions {

    protected RequestSpecification prepareRequest() {
        RestAssured.baseURI = Endpoints.BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.header("Accept", "application/json");
        request.header("Authorization", "Bearer " + ApiConnector.getAdminToken());
        return request;
    }
}
