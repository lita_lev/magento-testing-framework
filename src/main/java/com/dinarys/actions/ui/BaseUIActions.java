package com.dinarys.actions.ui;

import com.codeborne.selenide.Configuration;
import io.qameta.allure.Step;
import org.openqa.selenium.remote.BrowserType;

import static com.codeborne.selenide.Selenide.open;

public class BaseUIActions {

    @Step
    public GoogleActions openUrl(String url) {
        Configuration.browser = BrowserType.CHROME;
        open(url);
        return new GoogleActions();
    }
}
