package com.dinarys.actions.ui;

import com.dinarys.pages.GooglePage;
import io.qameta.allure.Step;
import org.testng.Assert;

public class GoogleActions {
    private GooglePage googlePage = new GooglePage();

    @Step
    public GoogleActions search(String query) {
        googlePage.search(query);
        return this;
    }

    @Step
    public GoogleActions verifySearchResults(String expectedResult) {
        String actualResult = googlePage.getResults().get(0).getText();
        Assert.assertEquals(actualResult, expectedResult);
        return this;
    }
}
