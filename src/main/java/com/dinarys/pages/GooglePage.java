package com.dinarys.pages;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.page;

public class GooglePage {

    public GooglePage search(String query) {
        $(By.name("q")).setValue(query).pressEnter();
        return page(GooglePage.class);
    }

    public ElementsCollection getResults() {
       return $$("#ires h3 > a");
    }
}
