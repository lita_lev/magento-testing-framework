package com.dinarys.tests;

import com.dinarys.actions.ui.BaseUIActions;
import org.testng.annotations.Test;

public class GoogleUITest extends BaseUITest {

    @Test
    public void googleSearchTest() {
        new BaseUIActions()
                .openUrl("http://google.com.ua")
                .search("Test")
                .verifySearchResults("test — failed");
    }


    @Test
    public void googleSearchTest2() {
        new BaseUIActions()
                .openUrl("http://google.com.ua")
                .search("Test")
                .verifySearchResults("est — Википедия");
    }
}
