package com.dinarys.tests;

import com.dinarys.actions.api.CustomerActions;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ApiTest {

    @Test
    public void apiAdminTest() {
        Assert.assertTrue(new CustomerActions().getAdmin().getId().equals("1"));
    }

    @Test
    public void apiCreateCustomerTest() {
        String email = String.format("autotest%s@gmail.com", RandomStringUtils.random(3, true, false));
        String firstName = String.format("AutoFirstName%s", RandomStringUtils.random(3, true, false));
        Assert.assertNotNull(new CustomerActions().createCustomer(email, firstName).getId());
    }
}
